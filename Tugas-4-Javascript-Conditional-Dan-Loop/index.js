// soal 1
// output B
var nilai;
nilai = 75;

if (nilai >= 85) {
   console.log("A");
} else if ((nilai >= 75) && (nilai < 85)){
   console.log("B");
} else if ((nilai >= 65) && (nilai < 75)){
   console.log("B");
} else if ((nilai >= 55) && (nilai < 65)){
   console.log("B");
} else {
   console.log("E");
}

// soal 2
// output: string  "21 Juni 2001"
var tanggal = 21;
var bulan = 6;
var tahun = 2001;

switch (bulan) {
   case 1:
         console.log(tanggal, "Januari", tahun);
         break;
   case 2:
         console.log(tanggal, "Februari", tahun);
         break;
   case 3:
         console.log(tanggal, "Maret", tahun);
         break;
   case 4:
         console.log(tanggal, "April", tahun);
         break;
   case 5:
         console.log(tanggal, "Mei", tahun);
         break;
   case 6:
         console.log(tanggal, "Juni", tahun);
         break;
   case 7:
         console.log(tanggal, "Juli", tahun);
         break;
   case 8:
      console.log(tanggal, "Agustus", tahun);
      break;
   case 9:
      console.log(tanggal, "September", tahun);
      break;
   case 10:
      console.log(tanggal, "Oktober", tahun);
      break;
   case 11:
      console.log(tanggal, "November", tahun);
      break;
   case 12:
      console.log(tanggal, "Desember", tahun);
      break;
   default:
      console.log("Tanggal lahir tidak ditemukan");
}

// soal 3
/*
Output untuk n=7 :

#
##
###
####
#####
######
####### */
var n = 7;
var output="";
for(var j=1;j<=n;j++){
    for(var k=1;k<=j;k++){
        output+="#"
    }
    console.log(output);
    output = "";
}

// soal 4
// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
// Output untuk m = 3
// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// Output untuk m = 5
// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript

var m = 10;
var jumlah = 1;
var batas ="";
while(jumlah <= m && m >= 1){
    if(jumlah%3==0){
        console.log(jumlah+" - I love VueJS");
        for(var n=0;n<jumlah;n++){
            batas += "=";           
        }
        console.log(batas)  
    } else if(jumlah%3==1){
        console.log(jumlah+" - I love programming");
    } else if(jumlah%3==2){
        console.log(jumlah+" - I love Javascript");
    }
    jumlah++; 
    batas ="";
}

