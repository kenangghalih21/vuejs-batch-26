// soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

const hitung_luas = (p, l) => {
   let luas = p*l
   console.log('Luas Persegi Panjang : '+luas)
}
const hitung_keliling = (p, l) => {
 let keliling = 2 * (p + l)
 console.log('Keliling Persegi Panjang : '+keliling)
}

const panjang = 4
const lebar = 6

hitung_luas(panjang, lebar);
hitung_keliling(panjang, lebar);

// soal 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
// const newFunction = function literal(firstName, lastName){
//    return {
//      firstName: firstName,
//      lastName: lastName,
//      fullName: function(){
//        console.log(firstName + " " + lastName)
//      }
//    }
//  }
  
//  //Driver Code 
//  newFunction("William", "Imoh").fullName() 

const newFunction = (firstName, lastName)=>{
   return {
     firstName,
     lastName,
     fullName:()=>{
       console.log(`${firstName} ${lastName}`)
     }
   }
 }
  //Driver Code 
 newFunction("William", "Imoh").fullName()

//  soal 3
const newObject = {
   firstName: "Muhammad",
   lastName: "Iqbal Mubarok",
   address: "Jalan Ranamanyar",
   hobby: "playing football",
 }
 
 const {firstName, lastName, address, hobby} = newObject
   
 console.log(firstName, lastName, address, hobby)

//  soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east];

console.log(combined)
   
// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
// const planet = "earth" 
// const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const planet = "earth" 
const view = "glass"
const sentence = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(sentence)