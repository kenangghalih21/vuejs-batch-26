// jawaban fix dari trainer
// soal 1
const luasPersegiPanjang = (panjang, lebar) => {
   const luas = panjang * lebar
   return luas
}

const kelilingPersegiPanjang = (panjang, lebar) => {
   const keliling = 2*(panjang + lebar)
   return keliling
}

console.log(kelilingPersegiPanjang(2,4))
console.log(luasPersegiPanjang(2,4))


// soal 2
const newFunction = (firstName, lastName) => {
   return {
     firstName,
     lastName,
     fullName:() => console.log(`${firstName} ${lastName}`)
   }
 }
  //Driver Code 
 newFunction("William", "Imoh").fullName()

//  soal 3
const newObject = {
   firstName: "Muhammad",
   lastName: "Iqbal Mubarok",
   address: "Jalan Ranamanyar",
   hobby: "playing football",
 }
 
 const {firstName, lastName, address, hobby} = newObject
   
 console.log(firstName, lastName, address, hobby);

//  soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]

console.log(combined)

// soal 5
const planet = "earth" 
const view = "glass"
const sentence = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(sentence)