// soal 1
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sortDaftar = daftarHewan.sort();
var length = sortDaftar.length;
for(var i = 0;i<length;i++){
    console.log(sortDaftar[i])
}

// soal 2
function introduce(data){
   let name = data.name 
   let age = data.age
   let address = data.address
   let hobby = data.hobby

   return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}`
}

var data = 
{
   name : "Ghalih" , 
   age : 30 , 
   address : "Jalan Yusuf" , 
   hobby : "Gaming" 
}
 
var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya Ghalih, umur saya 30 tahun, alamat saya di Jalan Yusuf, dan saya punya hobby yaitu Gaming" 

// soal 3
function hitung_huruf_vokal(input){
   var inputLowerCase = input.toLowerCase();
   var inputSplit = inputLowerCase.split("");
   var hitung = 0;
   for(i=0;i<inputSplit.length;i++){
       if(inputSplit[i] == "a"||inputSplit[i] == "i"||inputSplit[i] == "u"||inputSplit[i]=="e"||inputSplit[i] == "o"){
               hitung++;
       }
       
   }
   
   return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2);

// soal 4
function hitung(angka){
   let rumus = 2 * angka - 2;
   
   return rumus;
}

console.log( hitung(0) ); // -2
console.log( hitung(1) ); // 0
console.log( hitung(2) ); // 2
console.log( hitung(3) ); // 4
console.log( hitung(5) ); // 8
